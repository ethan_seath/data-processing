import pandas as pd
import pandas_datareader as web
from datetime import datetime
import sklearn.preprocessing as skp
import os

def label_df(df):
	"""
	Corrects the column lables of a dataframe

	Args:
		df (pandas dataframe): the dataframe to label

	Return:
		pandas dataframe: A correlctly labeled mplfinace ohlc dataframe
	"""
	df.index.name = 'Date'
	df.columns = ['Open', 'High', 'Low', 'Close', 'Volume']
	return df

def normalize_with_index(df):
	"""
	Normalizes data to a range 0 to 1 and corrects the column lables of a dataframe

	Args:
		df (pandas dataframe): the dataframe to normalize
	Returns:
		pandas dataframe: A normalized and correctly labled mplfinace ohlc dataframe
	"""
	index = df.index
	scalar = skp.MinMaxScaler()
	df = pd.DataFrame(scalar.fit_transform(df))
	df.index = index
	return label_df(df)

def get_df(ticker, API_KEY, start = None, end = None, normalize = False):
	"""
    Creates an ohlc dataframe based on a call to the IEX API. The dataframe
    has columns that are formatted to by used mplfinance. If the start and end arguements 
    are not used, the function opens a csv with the path data/'ticker'.csv and creates the ohlc
    dataframe based on that csv.

    Args:
        ticker (str): the stock ticker symbol
        API_KEY (str): the API Key to use
        start (str): the starting date, formatted "mm/dd/YYYY", default to None
        end (str): the ending date, formatted "mm/dd/YYYY", default to None

    Returns:
        pandas dataframe: A mplfinance formatted ohlc dataframe
    """
	if(start is None and end is None):
		if(not normalize):
			csv_df = pd.read_csv('data/' + ticker + '.csv', index_col = 0, parse_dates = True)
			return label_df(csv_df)
		else:
			return normalize_with_index(pd.read_csv('data/' + ticker + '.csv', index_col = 0, parse_dates = True))
	os.environ["IEX_API_KEY"] = API_KEY
	start_date = datetime.strptime(start, "%m/%d/%Y")
	end_date = datetime.strptime(end, "%m/%d/%Y")
	try:
		csv_df = web.DataReader(ticker, 'iex', start, end)
	except Exception as e:
		print("\nAPI ERROR:")
		print(e)
		print("")
		return None
	csv_df.to_csv('data/' + ticker + '.csv', index = True)
	csv_df = pd.read_csv('data/' + ticker + '.csv', index_col = 0, parse_dates = True)
	if(not normalize):
		return label_df(csv_df)
	else:
		return normalize_with_index(pd.read_csv('data/' + ticker + '.csv', index_col = 0, parse_dates = True)) 